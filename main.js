var page = require('webpage').create();

page.viewportSize = {
  width: 1280,
  height: 620
};

//page.settings.userAgent = 'PHANTOM 5000 (P) custom user agent| 1280x620';

page.onConsoleMessage = function(msg) {
  console.log(msg);
}


page.onLoadFinished = function(status) {
	
	setTimeout(function() {
		page.sendEvent('keypress', page.event.key.Enter);
    }, 500);
	
	setTimeout(function() {
		phantom.exit();
    }, 500);
	
};

page.open ('https://yourwebsite.test',function(status){
	if (status=="success"){
		
		page.evaluate(function() {
			var arr = document.getElementById("input");
			arr.value = "enter text here";
		});
		
	}else{
		console.log("Status : ???");
	}
});
